var clients = [];       //{username, socketId, index}
var sockets = []        //{socket, socketId, index}
var chatrooms = [];     //{roomName, creatorClient, users, isPrivate, password, temporarilyBannedClients, permanentlyBannedClients}

// Require the packages we will use:
var http = require("http"),
        socketio = require("socket.io"),
        fs = require("fs");

// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
        // This callback runs when a new connection is made to our HTTP server.

        fs.readFile("chat_client.html", function(err, data){
                // This callback runs when the client.html file has been read from the filesystem.

                if(err) return resp.writeHead(500);
                resp.writeHead(200);
                resp.end(data);
        });
});
app.listen(3456);

//Creates lobby that all users are ented in
var lobbyRoom = {
        "roomName" : "Lobby",
        "creatorClient" : null,
        "isPrivate" : false,
        "password" : null,
        "users" : [],
        "permanentlyBannedClients" : null
}
chatrooms.push(lobbyRoom);


// Do the Socket.IO magic:
var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
        // This callback runs when a new Socket.IO connection is established.
        socket.on('user_signin_to_server', function(data) {
                // This callback runs when the server receives a new message from the client.
                if (data["username"] == "") {
                        socket.emit("sign_in_error_message", {error:"Please enter a username before signing in"});
                        return;
                }
                for (var i = 0; i < clients.length; i++) {
                        if (data["username"] == clients[i].username) {
                                socket.emit("sign_in_error_message", {error:'The username "' + data["username"] + '" has already been taken.'});
                                return;
                        }
                }


                socket.username = data["username"];
                socket.room = "Lobby";
                socket.join("Lobby");

                

                console.log(data["username"] + " has signed in"); // log it to the Node.JS output
                var tempClient = {
                        "username" : data["username"],
                        "socketId" : socket.id,
                        "index" : clients.length
                }
                clients.push(tempClient);
                sockets.push({
                        "socket" : socket,
                        "socketId" : socket.id,
                        "index" : sockets.length
                });

                chatrooms[0].users.push(tempClient);
		
                socket.emit("sign_in_success", {client: tempClient, rooms: chatrooms, lobby: chatrooms[0]});
		socket.broadcast.to("Lobby").emit("load_new_user", {user: tempClient, room: chatrooms[0]});
		socket.broadcast.to(socket.room).emit("message_to_client", {message: socket.username+" has joined the room.", message_type: "room_update"});

        });

        socket.on('create_room_server', function(data) {
                //This callback runs when the server receives a create room request from the client.

                if (data["name"] == "") {
                 socket.emit("create_room_error_message", {error:"Please enter a room name"});
                        return;
                }

                for (var i = 0; i < chatrooms.length; i++) {
                        if (data["name"] == chatrooms[i].roomName) {
                                socket.emit("create_room_error_message", {error:'The room name "' + data["name"] + '" has already been taken.'});
                                return;
                        }
                }
		for (var i = 0; i < clients.length; i++) {
			if (clients[i].username == socket.username) {
				var user_index = i;
			}

		}
		
		
                var creatorClient = clients[user_index];
                if (data["password"] == "") {
                        var isPrivate = false;
                        var password = null;
                }
                else {
                        var isPrivate = true;
                        var password = data["password"];
                }

                console.log(creatorClient.username + " has created room: " + data["name"]); // log it to the Node.JS output

                var tempRoom = {
                        "roomName" : data["name"],
                        "creatorClient" : creatorClient, //Admin user
                        "isPrivate" : isPrivate,
                        "password" : password,
                        "users" : [],
                        "permanentlyBannedClients" : []
                }

                chatrooms.push(tempRoom);

                socket.emit("create_room_success", {room: tempRoom});
                io.sockets.to("Lobby").emit("load_new_room", {room: tempRoom, rooms: chatrooms});

        });

        socket.on('join_room_server', function(data) {
                //This callback runs when the server receives a join room request from the client.
		var newRoom = data["name"];
		var pass = data["password"];
		for(var i = 0; i < clients.length; i++) {
		    if (clients[i].username == socket.username) {
			    var user = clients[i];
		    }
		}
		for (var i = 0; i < chatrooms.length; i++){
		    if (chatrooms[i].roomName == newRoom){
			var index = i;
		    }
		    if (chatrooms[i].roomName == socket.room){
			var oldIndex = i;
		    }
		}
		for (var j = 0; j < chatrooms[index].permanentlyBannedClients.length; j++){
		    
		    if(chatrooms[index].permanentlyBannedClients[j] == socket.username){
			socket.emit("join_room_error_message", {error:'You have been permanently banned from room "' + data["name"] + '".'});
			
			return;
		    }
		}
		if (chatrooms[index].isPrivate){
		    if(chatrooms[index].password != pass){
			    socket.emit("join_room_error_message", {error:'The password you entered to join room "' + data["name"] + '" is incorrect.'});
			    return;
		    }
			
			
		}
		var indexInChat;
		for(var j = 0; j < chatrooms[oldIndex].users.length; j++) {
		    if (chatrooms[oldIndex].users[j].username == socket.username) {
			    indexInChat = j;
		    }
		}
		
		for (var i = 0; i < clients.length; i++) {
		    if (clients[i].username == socket.username) {
			    var userIndex = i;
		    }
		}
		var tempClient = clients[userIndex];
		socket.broadcast.to(socket.room).emit("remove_user_from_room", {user: tempClient});
		socket.broadcast.to(socket.room).emit("message_to_client", {message: socket.username+" has left the room.", message_type: "room_update"});
		chatrooms[oldIndex].users.splice(indexInChat, 1);
		socket.leave(socket.room);
                socket.join(newRoom);
                socket.room = newRoom;
                console.log(user.username + " has joined room: " + data["name"]);
		 
		socket.broadcast.to(socket.room).emit("message_to_client", {message: socket.username+" has joined the room.", message_type: "room_update"});
                chatrooms[index].users.push(user);
		socket.emit("join_room_success", {room: chatrooms[index]});
		socket.broadcast.to(socket.room).emit("load_new_user", {user: tempClient, room: chatrooms[index]});


        });

	socket.on('request_leave_room_to_server', function() {
		//This callback runs when the server receives a leave room request from the client or the room is being deleted.
		var room_index;
		var user_index;
		for (var i = 0; i < chatrooms.length; i++){
			console.log(chatrooms[i].roomName);
		    if (chatrooms[i].roomName == socket.room){
			room_index = i;
		    }
		}
		if (room_index != undefined) {
			for (var i = 0; i < chatrooms[room_index].users.length; i++){
				if (chatrooms[room_index].users[i].username == socket.username){
				    user_index = i;
				}
			}
			var tempClient = chatrooms[room_index].users[user_index];
		}
		else{
			for (var i = 0; i < clients.length; i++) {
				if (clients[i].username == socket.username) {
					var tempClient = clients[i];
				}
			}
		}
		var deletedRoom = socket.room;
		socket.broadcast.to(socket.room).emit("remove_user_from_room", {user: tempClient});
		socket.broadcast.to(socket.room).emit("message_to_client", {message: socket.username+" has left the room.", message_type: "room_update"});
		socket.leave(socket.room);
		socket.join("Lobby");
		socket.room="Lobby";
		socket.broadcast.to(socket.room).emit("message_to_client", {message: socket.username+" has joined the room.", message_type: "room_update"});
		chatrooms[0].users.push(tempClient);
		
		socket.emit("leave_room_success", {rooms: chatrooms});
		socket.broadcast.to(socket.room).emit("load_new_user", {user: tempClient, room: chatrooms[0]});
		if (room_index != undefined) {
			chatrooms[room_index].users.splice(user_index, 1);
		}
		else{
			socket.emit("message_to_client", {message: deletedRoom+" has been deleted.", message_type: "room_update"});
		}
	});
	
        socket.on('message_to_server', function(data) {
                // This callback runs when the server receives a new message from the client.
		
                console.log("message: "+data["message"]); // log it to the Node.JS output
                socket.to(socket.room).emit("message_to_client",{message:data["message"], message_type: "message_to_room", sender_username: socket.username }); // broadcast the message to other users
		socket.emit("message_to_client",{message:data["message"], message_type: "message_to_room", sender_username: socket.username });
        });
	
	socket.on('private_message_to_server', function(data) {
                // This callback runs when the server receives a new message from the client.

                console.log("private message: "+data["message"]); // log it to the Node.JS output
                for (var i = 0; i < clients.length; i++) {
			if (clients[i].username == data["username"]) {
				var tempClient = clients[i];
			}
		}
		io.to(tempClient.socketId).emit("message_to_client", {message:data["message"], message_type: "private_message", sender_username: socket.username, receiver_username:tempClient.username });
		socket.emit("message_to_client", {message:data["message"], message_type: "private_message", sender_username: socket.username, receiver_username:tempClient.username });
        });

        socket.on('kicker', function(data){
		//This callback runs when the server receives a kick request from the client on a specified user.
		var room_index;
		var user_index;
		for (var i = 0; i < chatrooms.length; i++){
		    if (chatrooms[i].roomName == socket.room){
			room_index = i;
		    }
		}
		for (var i = 0; i < chatrooms[room_index].users.length; i++){
		    if (chatrooms[room_index].users[i].username == data["username"]){
			user_index = i;
		    }
		}
		for (var i = 0; i < clients.length; i++) {
			if (clients[i].username == data["username"]) {
				var userId = clients[i].socketId;
			}	
		}	
		if (data["kick_type"] == "Permanantly Ban") {
			chatrooms[room_index].permanentlyBannedClients.push(data["username"]);
			console.log(data["username"] +" banned by "+ socket.username);
		}
		else{
			console.log(data["username"] + " kicked by " + socket.username);
		}
		
		
		var tempClient = chatrooms[room_index].users[user_index];
		
		socket.broadcast.to("Lobby").emit("message_to_client", {message: data["username"]+" has joined the room.", message_type: "room_update"});
		socket.broadcast.to("Lobby").emit("load_new_user", {user: tempClient, room: chatrooms[0]});
		console.log(tempClient.username + " IS BEING KICKED");
		io.sockets.connected[userId].leave(socket.room);
		io.sockets.connected[userId].join("Lobby");
		io.sockets.connected[userId].room="Lobby";
		io.sockets.connected[userId].emit("leave_room_success", {rooms: chatrooms});
		io.sockets.to(socket.room).emit("remove_user_from_room", {user: chatrooms[room_index].users[user_index]});
		io.sockets.to(socket.room).emit("message_to_client", {message: data["username"]+" has been removed from the room.", message_type: "room_update"});
		chatrooms[0].users.push(tempClient);
		
		chatrooms[room_index].users.splice(user_index, 1);
		
		
        });

        socket.on('disconnect', function() {
            //This callback runs when the server sees that a client has disconected from the server.
	    
	    
            var room_index;
            var user_index;
            for (var i = 0; i < chatrooms.length; i++){
                if (chatrooms[i].roomName == socket.room){
                    var room_index = i;
                }
            }
            
	    
	    var indexInChat;
	    if (chatrooms[room_index] == undefined) {
		return;
	    }
	    for(var j = 0; j < chatrooms[room_index].users.length; j++) {
		if (chatrooms[room_index].users[j].username == socket.username) {
			indexInChat = j;
		}
	    }
	    socket.broadcast.to(socket.room).emit("remove_user_from_room", {user: chatrooms[room_index].users[indexInChat]});
	    socket.broadcast.to(socket.room).emit("message_to_client", {message: socket.username+" has disconnected.", message_type: "room_update"});
            chatrooms[room_index].users.splice(indexInChat, 1);
            console.log(socket.username + " has left room: " + socket.room);
	    for (var i = 0; i < clients.length; i++){
                if (clients[i].username == socket.username){
		    clients.splice(i, 1);
                }
            }
	    
        });
	
	socket.on('delete_room', function(data){
		//This callback runs when the server receives a delete room request from the client.
		for (var i = 0; i < chatrooms.length; i++){
			if (chatrooms[i].roomName == data["roomname"]){
			    var room_index = i;
			}
		}	
		var deletedRoom = chatrooms[room_index];
		
		io.sockets.in(deletedRoom.roomName).emit("request_leave_room_to_server");
		io.sockets.to(socket.room).emit("message_to_client", {message: chatrooms[room_index].roomName+" has been deleted.", message_type: "room_update"});
		io.sockets.to(socket.room).emit("remove_room", {room: deletedRoom});
		
		chatrooms.splice(room_index, 1);
		
	});


});
